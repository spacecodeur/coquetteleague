# Installation et utilisation de raylib : CLIENT LOURD

## Installation/Compilation du moteur de jeu raylib sur linux (debian)

Installation du moteur [raylib](https://www.raylib.com/) et compilation de ce dernier (qui reprend les commandes qui se trouvent dans https://github.com/raysan5/raylib/wiki/Working-on-GNU-Linux)

```
sudo apt install build-essential git \
&& sudo apt install cmake \
&& sudo apt install libasound2-dev libx11-dev libxrandr-dev libxi-dev libgl1-mesa-dev libglu1-mesa-dev libxcursor-dev libxinerama-dev \
&& mkdir -p ~/code/lib \
&& cd ~/code/lib \
&& git clone https://github.com/raysan5/raylib.git raylib \
&& cd raylib/src/ \
&& make PLATFORM=PLATFORM_DESKTOP RAYLIB_LIBTYPE=SHARED \
&& sudo make install RAYLIB_LIBTYPE=SHARED
```

## Compilation du code du jeu

```
g++ main.cpp -o bin/game.bin -lraylib
```

# Installation et utilisation de raylib : CLIENT LÉGER (browser web)

Je me base sur la [documentation suivante](https://github.com/raysan5/raylib/wiki/Working-for-Web-(HTML5))

## Installation de emscripten

- emscripten permet de compiler du C++ en web assembly 

```
sudo apt install python3 git \
&& sudo apt install cmake \
&& mkdir -p ~/code/lib \
&& cd ~/code/lib \
&& git clone https://github.com/emscripten-core/emsdk.git emsdk \
&& cd emsdk \
&& # Download and install the latest SDK tools.
./emsdk install latest \
&& # Make the "latest" SDK "active" for the current user. (writes .emscripten file)
./emsdk activate latest \    
&& # Activate PATH and other environment variables in the current terminal
source ./emsdk_env.sh
```

- Exécuter la commande suivante pour voir si l'installation de emscripten est good : `emcc -v`

## Je n'ai jamais installé/compilé raylib (pour CLIENT LOURD comme pour CLIENT LÉGER)

Installer en local raylib : 

```
sudo apt install build-essential git \
&& sudo apt install cmake \
&& sudo apt install libasound2-dev libx11-dev libxrandr-dev libxi-dev libgl1-mesa-dev libglu1-mesa-dev libxcursor-dev libxinerama-dev \
&& mkdir -p ~/code/lib \
&& cd ~/code/lib \
&& git clone https://github.com/raysan5/raylib.git raylib \
```

## Compilation le moteur du jeu raylib pour fonctionner pour le Web

```
cd ~/code/lib/raylib/src \
&& emcc -c rcore.c -Os -Wall -DPLATFORM_WEB -DGRAPHICS_API_OPENGL_ES2 \
&& emcc -c rshapes.c -Os -Wall -DPLATFORM_WEB -DGRAPHICS_API_OPENGL_ES2 \
&& emcc -c rtextures.c -Os -Wall -DPLATFORM_WEB -DGRAPHICS_API_OPENGL_ES2 \
&& emcc -c rtext.c -Os -Wall -DPLATFORM_WEB -DGRAPHICS_API_OPENGL_ES2 \
&& emcc -c rmodels.c -Os -Wall -DPLATFORM_WEB -DGRAPHICS_API_OPENGL_ES2 \
&& emcc -c utils.c -Os -Wall -DPLATFORM_WEB \
&& emcc -c raudio.c -Os -Wall -DPLATFORM_WEB \
&& emar rcs libraylib.a rcore.o rshapes.o rtextures.o rtext.o rmodels.o utils.o raudio.o
```

Un fichier nommé `libraylib.a` (dans `~/code/lib/raylib/src`) a été généré et il contient le moteur de raylib compilé pour le web !

## Compilation du code du jeu 

Se rendre tout d'abord dans le dossier du projet (du jeu), puis exécuter la commande suivante :

`mkdir -p web && emcc -o web/game.html main.cpp -Os --preload-file assets -Wall ${HOME}/code/lib/raylib/src/libraylib.a -I${HOME}/code/lib/raylib/src -s USE_GLFW=3 -s ASYNCIFY --shell-file ${HOME}/code/lib/raylib/src/shell.html -DPLATFORM_WEB`

### Attention si vous exécutez le fichier `web/game.html` sur votre navigateur en local

Exécutez le fichier `web/game.html` via la commande suivante : `emrun --browser firefox web/game.html`

(firefox devrais pouvoir être changé en chrome dans la commande je suppose)