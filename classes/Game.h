#include "element/child/Player.h"
#include "land/Land.h"
#include <random>

class Game
{
    public:

        int state = 1; // 0 = homescreen, 1 = jeu en cours, 2 = game over

        void nextState()
        {
            this->state += 1;
        }

        Land createLand()
        {

            Land land = Land();
            land.addGround();

            land.addPlatform(0, land.Y_LEVEL_1, 200);
            land.addBonus(0, land.Y_LEVEL_1, 20);

            land.addPlatform(300, land.Y_LEVEL_2, 200);
            land.addBonus(300, land.Y_LEVEL_2, 20);

            land.addPlatform(370, land.Y_LEVEL_1, 200);
            land.addBonus(370, land.Y_LEVEL_1, 20);

            land.addPlatform(350, land.Y_LEVEL_3, 200);
            land.addBonus(350, land.Y_LEVEL_3, 20);
            
            land.addPlatform(500, land.Y_LEVEL_1, 200);
            land.addBonus(500, land.Y_LEVEL_1, 20);
            
            land.addPlatform(650, land.Y_LEVEL_3, 200);
            land.addBonus(650, land.Y_LEVEL_3, 20);

            land.addPlatform(800, land.Y_LEVEL_2, 200);
            land.addBonus(800, land.Y_LEVEL_2, 20);
            
            land.addPlatform(850, land.Y_LEVEL_1, 300);

            land.addPlatform(950, land.Y_LEVEL_2, 150);
            land.addBonus(950, land.Y_LEVEL_2, 20);

            return land;
        }

        Player createPlayer(int groundY)
        {
            Player player = Player(groundY);
            return player;
        }

};