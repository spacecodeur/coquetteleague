#ifndef PLAYER_H
#define PLAYER_H

#include "raylib.h"
#include "../../physic/Physic.h"

extern const float SCREEN_WIDTH;
extern const float SCREEN_HEIGHT;

class Player
{
public:
    float speed = 200;
    float gravity = 350;
    float jumpSpeed = 40;
    bool canJump = true;
    int yBeforeJump = 0;
    int yHighestAfterJump = 0;

    Vector2 position = {this->x, this->y};
    float x = SCREEN_WIDTH / 2;
    float y;

    // Texture2D spritesheet = LoadTexture("assets/sprites/Shinobi/Walk.png");       SPRITE ORIGINAL A REMETTRE
    Texture2D spritesheet = LoadTexture("assets/sprites/shinobi.png");                                  //     TEST
    Rectangle sourceRect = {this->x, this->y, (float)spritesheet.width / 8, (float)spritesheet.height}; // Division par le nombre de frames
    int currentFrame = 0;
    int maxFrames = 8; // Nombre total de frames dans la spritesheet

    Player(int groundY)
    {
        this->y = groundY - this->spritesheet.height;
    }

    // sert pour simplifier le code coté main.cpp / collision (physic)
    Block toBlock()
    {
        Block block;
        block.x = this->x;
        block.y = this->y;
        block.width = this->spritesheet.width;
        block.height = this->spritesheet.height;
        return block;
    }

    void move(bool *allowedMoves, float deltaTime)
    {
        if (IsKeyPressed(KEY_SPACE))
        {
            this->yBeforeJump = this->y;
        }
        this->yHighestAfterJump = this->yBeforeJump - 230;

        if (IsKeyDown(KEY_SPACE) && allowedMoves[0] == true && this->y > this->yHighestAfterJump && this->canJump == true)
        {

            if (this->y < (this->yHighestAfterJump + 1))
            {
                this->canJump = false;
            }

            this->y -= this->jumpSpeed;
        }
        if (allowedMoves[2] == false)
        {
            this->canJump = true;
        }

        if (IsKeyDown(KEY_RIGHT) && allowedMoves[1] == true)
        {
            currentFrame++;
            sourceRect.x = currentFrame * spritesheet.width / 8;
            this->x += this->speed * deltaTime;
        }
        if (IsKeyDown(KEY_DOWN) && allowedMoves[2] == true)
        {
            this->y += this->speed * deltaTime;
        }
        if (IsKeyDown(KEY_LEFT) && allowedMoves[3] == true)
        {
            this->x -= this->speed * deltaTime;
            currentFrame++;
            sourceRect.x = currentFrame * spritesheet.width / 8;
        }
        if (this->x > SCREEN_WIDTH)
        {
            this->x = SCREEN_WIDTH - 10;
        }
        else if (this->y > SCREEN_HEIGHT)
        {
            this->y = SCREEN_HEIGHT - 10;
        }
        else if (this->x < 0)
        {
            this->x = 10;
        }
        else if (this->y < 0)
        {
            this->y = 10;
        }

        // Gestion de l'animation
        if (currentFrame >= maxFrames)
        {
            currentFrame = 0;
        }

        sourceRect.x = (float)currentFrame * (float)spritesheet.width / 8;

        if (allowedMoves[2] == true) ///  GRAVITY
        {
            this->y += this->gravity * deltaTime;
        }

        position = {this->x, this->y};
    }
};

#endif