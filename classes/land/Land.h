#ifndef LAND_H
#define LAND_H

#include "envItem/child/Bonus.h"
#include "envItem/child/Obstacle.h"
#include "raylib.h"
#include <vector>

extern const float SCREEN_HEIGHT;

class Land
{
public:
    std::vector<Obstacle> obstacles;
    std::vector<Bonus> bonuses;
    int groundY = SCREEN_HEIGHT - 100;
    enum YLevelFromGround { Y_LEVEL_1 = 1, Y_LEVEL_2 = 3, Y_LEVEL_3 = 5 };

    ~Land() = default;

    void addGround()
    {
        obstacles.emplace_back(-1000, groundY, 20000, 100, LIGHTGRAY);
    }

    void addPlatform(int x, YLevelFromGround yLevelFromGround, int width)
    {
        obstacles.emplace_back(x, calculatePlatformY(yLevelFromGround), width, 10, BLUE);
    }

    void addBonus(int x, YLevelFromGround yLevelFromGround, int value)
    {
        bonuses.emplace_back(x + 85 , calculatePlatformY(yLevelFromGround) - 50, 30, 30, value);
    }

    std::vector<Obstacle>& getObstaclesForModification()
    {
        return obstacles;
    }

    std::vector<Bonus>& getBonusesForModification()
    {
        return bonuses;
    }


private:
    int calculatePlatformY(YLevelFromGround yLevelFromGround) const
    {
        return groundY * 0.9 - static_cast<int>(groundY / 8 * yLevelFromGround);
    }
};

#endif