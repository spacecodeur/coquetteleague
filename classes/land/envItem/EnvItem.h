#ifndef ENVITEM_H
#define ENVITEM_H

#include "raylib.h"
#include "../../physic/Physic.h"

class EnvItem
{
    public:
        int x;
        int y;
        int width;
        int height;
        Color color;
        bool isBlocking;

    EnvItem() : x(0), y(0), width(0), height(0), color(WHITE), isBlocking(true){}

    EnvItem(int x, int y, int width, int height, Color color, bool isBlocking)
    {
        this->x = x;
        this->y = y;
        this->width = width;
        this->height = height;
        this->color = color;
        this->isBlocking = isBlocking;
    }

    Block toBlock() const
    {
        Block block;
        block.x = this->x;
        block.y = this->y;
        block.width = this->width;
        block.height = this->height;
        return block;
    }
};

#endif 