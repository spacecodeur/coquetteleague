#ifndef BONUS_H
#define BONUS_H

#include "raylib.h"
#include "../EnvItem.h"

class Bonus : public EnvItem
{
public:
    int value;

    Bonus() : EnvItem(), value(0) {}

    Bonus(int x, int y, int width, int height, int value) : EnvItem(x, y, width, height, GREEN, false)
    {
        this->value = value;
    }

    int getValue() const
    {
        return this->value;
    }
};

#endif 