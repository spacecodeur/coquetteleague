#ifndef OBSTACLE_H
#define OBSTACLE_H

#include "raylib.h"
#include "../EnvItem.h"

class Obstacle : public EnvItem
{
    public:
        Obstacle() : EnvItem() {}

        Obstacle(int x, int y, int width, int height, Color color) : EnvItem(x, y, width, height, color, true) {}
};

#endif 