#ifndef PHYSIC_H
#define PHYSIC_H

struct Block {
    float x;
    float y;
    float width;
    float height;
};

class Physic
{
    public:

        /*
           -1 : pas de collision
            0 : collision source up     / target bottom
            1 : collision source right  / target left
            2 : collision source bottom / target up
            3 : collision source left   / target right
        */
        static int isInColision(Block blockSource, Block blockTarget)
        {
            /*
            if(blockSource.y == blockTarget.y + blockTarget.height)
            {
                return 0;
            }
            */
            /*
            if (
                blockSource.x + blockSource.width > blockTarget.x
                &&
                blockSource.y + blockSource.height > blockTarget.y
                &&
                blockSource.y < blockTarget.y + blockTarget.height
            )
            {
                return 1;
            }
            */
            if(
                blockSource.y + blockSource.height >= blockTarget.y
                && 
                blockSource.x > blockTarget.x - blockSource.width
                &&
                blockSource.x < blockTarget.x + blockTarget.width
                &&
                blockSource.y < blockTarget.y
                && 
                blockSource.y + blockSource.height < blockTarget.y + blockTarget.height
                &&
                ! IsKeyDown(KEY_DOWN)
            )
            {
                return 2;
            }
            /*
            if(blockSource.x == blockTarget.x + blockTarget.width)
            {
                return 3;
            }
            */
            else // pas de collision
            {
                return -1;
            }
        }
};

#endif 