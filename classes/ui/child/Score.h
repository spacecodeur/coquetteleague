class Score
{
private:
    int value;

public:
    Score()
    {
        this->value = 0;
    }

    void increaseValue(int value)
    {
        this->value += value;
    }

    int getValue()
    {
        return this->value;
    }
};