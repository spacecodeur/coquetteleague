#include "raylib.h"
#include "classes/Game.h"
#include "classes/physic/Physic.h"

// variables globales

const float SCREEN_WIDTH = 1600.0;
const float SCREEN_HEIGHT = 900.0;

// je commence un petit bout de jeu en C++ avec de la POO par ici ;)

int main()
{
    int frame = 0;
    const float updateTime(1.f/12.f);

    InitWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "Coquetteleague");

    // instanciation de Game
    Game game = Game();

    // Page d'accueil du jeu
    while (game.state == 0 && !WindowShouldClose())
    {
        // TODO: implémenter l'écran d'accueil
        game.nextState(); // pour le moment on skip directement
    }

    Land land = game.createLand();
    Player player = game.createPlayer(land.groundY);

    std::vector<Obstacle>& obstaclesForModification = land.getObstaclesForModification();
    int nbrObstacles = obstaclesForModification.size();

    std::vector<Bonus>& bonusesForModification = land.getBonusesForModification();
    int nbrBonuses = bonusesForModification.size();

    int score = 0;

    SetTargetFPS(60);

    // le jeu est en cours
    while (game.state == 1 && !WindowShouldClose())
    {
        float deltaTime = GetFrameTime();

        BeginDrawing();

            ClearBackground(RAYWHITE); 
                
            bool allowedMoves[4] = {true, true, true, true};
            for (int i = 0; i < nbrObstacles; i++)
            {
                DrawRectangle(obstaclesForModification[i].x, obstaclesForModification[i].y, obstaclesForModification[i].width, obstaclesForModification[i].height, obstaclesForModification[i].color);
                int collisionState = Physic::isInColision(player.toBlock(), obstaclesForModification[i].toBlock());

                if (collisionState != -1)
                {
                    allowedMoves[collisionState] = false;
                }
            }

            for (int i = 0; i < nbrBonuses; i++)
            {
                DrawRectangle(bonusesForModification[i].x, bonusesForModification[i].y, bonusesForModification[i].width, bonusesForModification[i].height, bonusesForModification[i].color);
                int collisionState = Physic::isInColision(player.toBlock(), bonusesForModification[i].toBlock());
                if (collisionState != -1)
                {
                    bonusesForModification[i].color = RAYWHITE;
                    score += bonusesForModification[i].value;
                    bonusesForModification[i].value = 0;
                }
            }
                
            DrawText(TextFormat("SCORE: %i", score), 20, 20, 40, RED);

            player.move(allowedMoves, deltaTime);
            DrawTextureV(player.spritesheet, player.position, WHITE);

        EndDrawing();
    }

    while (game.state == 2 && !WindowShouldClose())
    {
        break; // TODO : implémenter l'écran game over
    }

    CloseWindow();

    return 0;
}